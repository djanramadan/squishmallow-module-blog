define([
    'jquery',
    'SquishMallow_Blog/js/aos.min',
  ],function($, AOS){
    $(document).ready(function(){
       AOS.init({
        offset: -50,
        duration: 1000
       });
    });
  });